package controller

import (
    "context"
    networkingv1 "k8s.io/api/networking/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/runtime"
    ctrl "sigs.k8s.io/controller-runtime"
    "sigs.k8s.io/controller-runtime/pkg/client"
    "sigs.k8s.io/controller-runtime/pkg/log"
    "gitlab.com/optimus3435034/snmp-operator/internal/variables"
    monitorv1 "gitlab.com/optimus3435034/snmp-operator/api/v1"
)

type IngressReconciler struct {
    client.Client
    Scheme *runtime.Scheme
}

func (r *IngressReconciler) ReconcileIngress(ctx context.Context, mc *monitorv1.MonitorConfig) error {
    logger := log.FromContext(ctx)
    logger.Info("Starting reconciliation of Ingress", "Ingress.Name", mc.Name, "Namespace", mc.Namespace)

    ingress := &networkingv1.Ingress{
        ObjectMeta: metav1.ObjectMeta{
            Name:      mc.Name,
            Namespace: mc.Namespace,
            Annotations: make(map[string]string), 
        },
    }

    _, err := ctrl.CreateOrUpdate(ctx, r.Client, ingress, func() error {
        logger.Info("Updating Ingress spec for", "Ingress.Name", mc.Name)
        
        // Ensure the annotations map is initialized before assignment
        if ingress.ObjectMeta.Annotations == nil {
            ingress.ObjectMeta.Annotations = make(map[string]string)
        }
        
        ingress.ObjectMeta.Annotations["kubernetes.io/ingress.class"] = "nginx"
        
        pathType := networkingv1.PathTypeImplementationSpecific
        ingress.Spec = networkingv1.IngressSpec{
            Rules: []networkingv1.IngressRule{
                {
                    Host: variables.IngressHost,
                    IngressRuleValue: networkingv1.IngressRuleValue{
                        HTTP: &networkingv1.HTTPIngressRuleValue{
                            Paths: []networkingv1.HTTPIngressPath{
                                {
                                    Path: "/",
                                    PathType: &pathType,
                                    Backend: networkingv1.IngressBackend{
                                        Service: &networkingv1.IngressServiceBackend{
                                            Name: mc.Name,
                                            Port: networkingv1.ServiceBackendPort{
                                                Number: variables.ServicePort,
                                            },
                                        },
                                    },
                                },
                            },
                        },
                    },
                },
            },
        }
        return ctrl.SetControllerReference(mc, ingress, r.Scheme)
    })

    if err != nil {
        logger.Error(err, "Failed to reconcile Ingress", "Ingress.Name", mc.Name)
        return err
    }

    logger.Info("Successfully reconciled Ingress", "Ingress.Name", mc.Name)
    return nil
}

