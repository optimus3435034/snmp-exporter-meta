package controller

import (
    "context"
    appsv1 "k8s.io/api/apps/v1"
    corev1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/runtime"
    ctrl "sigs.k8s.io/controller-runtime"
    "sigs.k8s.io/controller-runtime/pkg/client"
    "sigs.k8s.io/controller-runtime/pkg/log"
    monitorv1 "gitlab.com/optimus3435034/snmp-operator/api/v1"
    variables "gitlab.com/optimus3435034/snmp-operator/internal/variables"
)

type DeploymentReconciler struct {
    client.Client
    Scheme *runtime.Scheme
}

func (r *DeploymentReconciler) ReconcileDeployment(ctx context.Context, mc *monitorv1.MonitorConfig) error {
    logger := log.FromContext(ctx)
    logger.Info("Starting reconciliation of Deployment", "Deployment.Name", mc.Name, "Namespace", mc.Namespace)

    deployment := &appsv1.Deployment{
        ObjectMeta: metav1.ObjectMeta{
            Name:      mc.Name,
            Namespace: mc.Namespace,
        },
    }

    _, err := ctrl.CreateOrUpdate(ctx, r.Client, deployment, func() error {
        logger.Info("Updating deployment spec for", "Deployment.Name", mc.Name)

        deployment.Spec = appsv1.DeploymentSpec{
            Replicas: &variables.ReplicaNum,
            Selector: &metav1.LabelSelector{
                MatchLabels: map[string]string{"app": mc.Name},
            },
            Template: corev1.PodTemplateSpec{
                ObjectMeta: metav1.ObjectMeta{
                    Labels: map[string]string{"app": mc.Name},
                },
                Spec: corev1.PodSpec{
                    Containers: []corev1.Container{
                        {
                            Name:  mc.Name,
                            Image: variables.Image,
                            Ports: []corev1.ContainerPort{{ContainerPort: variables.ContainerPort}},
                        },
                    },
                },
            },
        }
        return ctrl.SetControllerReference(mc, deployment, r.Scheme)
    })

    if err != nil {
        logger.Error(err, "Failed to reconcile Deployment", "Deployment.Name", mc.Name)
        return err
    }

    logger.Info("Successfully reconciled Deployment", "Deployment.Name", mc.Name)
    return nil
}

