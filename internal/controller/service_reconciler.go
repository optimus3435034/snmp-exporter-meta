package controller

import (
    "context"
    "k8s.io/apimachinery/pkg/util/intstr"
    corev1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    ctrl "sigs.k8s.io/controller-runtime"
    "k8s.io/apimachinery/pkg/runtime"
    monitorv1 "gitlab.com/optimus3435034/snmp-operator/api/v1"
    "sigs.k8s.io/controller-runtime/pkg/client"
    "sigs.k8s.io/controller-runtime/pkg/log"
    variables "gitlab.com/optimus3435034/snmp-operator/internal/variables"
)

type ServiceReconciler struct {
    client.Client
    Scheme *runtime.Scheme
}

func (r *ServiceReconciler) ReconcileService(ctx context.Context, mc *monitorv1.MonitorConfig) error {
    logger := log.FromContext(ctx)
    logger.Info("Starting reconciliation of Service", "Service.Name", mc.Name, "Namespace", mc.Namespace)

    service := &corev1.Service{
        ObjectMeta: metav1.ObjectMeta{
            Name:      mc.Name,
            Namespace: mc.Namespace,
        },
    }

    _, err := ctrl.CreateOrUpdate(ctx, r.Client, service, func() error {
        logger.Info("Updating Service spec for", "Service.Name", mc.Name)
        service.Spec = corev1.ServiceSpec{
            Selector: map[string]string{"app": mc.Name},
            Ports: []corev1.ServicePort{
                {
                    Port:       variables.ServicePort,
                    TargetPort: intstr.FromInt(variables.ContainerPort),
                },
            },
        }
        return ctrl.SetControllerReference(mc, service, r.Scheme)
    })

    if err != nil {
        logger.Error(err, "Failed to reconcile Service", "Service.Name", mc.Name)
        return err
    }

    logger.Info("Successfully reconciled Service", "Service.Name", mc.Name)
    return nil
}

