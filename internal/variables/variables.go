package variables
import "k8s.io/api/networking/v1"

var ReplicaNum int32 = 1
var ImplementationSpecific = v1.PathTypeImplementationSpecific  // Correctly typed as networkingv1.PathType

const (
    Image          = "avimorgm/aggr-prod:latest" // Example image
    ContainerPort  = 8080
    ServicePort    = 80
    IngressHost    = "aggr.optimus.local"
)

