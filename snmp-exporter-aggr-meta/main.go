package main

import (
    "flag"
    "fmt"
    "io"
    "log"
    "net/http"
    "os"
    "strconv"
    "strings"
    "sync"
    "time"

    "github.com/prometheus/common/expfmt"
    "github.com/prometheus/client_model/go"
)

type Config struct {
    Server  struct{ Bind string }
    Timeout int
    Targets []string
}

var (
    targetLabelsEnabled *bool
    targetLabelName     *string
    serverBind          *string
    targetScrapeTimeout *int
    targets             *string
    dynamicRegistration *bool
    verboseFlag         *bool
)

func stringFlag(fs *flag.FlagSet, name string, value string, usage string) *string {
    return fs.String(name, value, usage)
}

func intFlag(fs *flag.FlagSet, name string, value int, usage string) *int {
    return fs.Int(name, value, usage)
}

func boolFlag(fs *flag.FlagSet, name string, value bool, usage string) *bool {
    return fs.Bool(name, value, usage)
}

func init() {
    serverBind = stringFlag(flag.CommandLine, "server.bind", ":8080", "Bind the HTTP server to this address")
    targetScrapeTimeout = intFlag(flag.CommandLine, "targets.scrape.timeout", 1000, "If a target metrics pages does not responde with this many miliseconds then timeout")
    targets = stringFlag(flag.CommandLine, "targets", "", "comma separated list of targets e.g. http://localhost:8081/metrics,http://localhost:8082/metrics or url1=http://localhost:8081/metrics,url2=http://localhost:8082/metrics for custom label values")
    targetLabelsEnabled = boolFlag(flag.CommandLine, "targets.label", true, "Add a label to metrics to show their origin target")
    targetLabelName = stringFlag(flag.CommandLine, "targets.label.name", "ae_source", "Label name to use if a target name label is appended to metrics")
    dynamicRegistration = boolFlag(flag.CommandLine, "targets.dynamic.registration", true, "Enabled dynamic targets registration/deregistration using /register and /unregister endpoints")
    verboseFlag = boolFlag(flag.CommandLine, "verbose", false, "Enable verbose logging")
}

func main() {
    flag.Parse()

    config := &Config{
        Server: struct{ Bind string }{Bind: *serverBind},
        Timeout: *targetScrapeTimeout,
        Targets: filterEmptyStrings(strings.Split(*targets, ",")),
    }

    if len(config.Targets) < 1 {
        if *dynamicRegistration {
            log.Print("Working With Dynamic Registration for SNMP Pods")
        } else {
            log.Fatal("Dynamic Registration is disabled, exit the application")
            os.Exit(1)
        }
    }

    aggregator := &Aggregator{HTTP: &http.Client{Timeout: time.Duration(config.Timeout) * time.Millisecond}}

    targets := NewTargets(config.Targets)

    mux := http.NewServeMux()
    mux.HandleFunc("/metrics", func(rw http.ResponseWriter, r *http.Request) {
        defer r.Body.Close()
        err := r.ParseForm()
        if err != nil {
            http.Error(rw, "Bad Request", http.StatusBadRequest)
            return
        }
        if t := r.Form.Get("t"); t != "" {
            targetKey := t
            idx, err := strconv.Atoi(targetKey)
            if err != nil {
                http.Error(rw, "Bad Request", http.StatusBadRequest)
                return
            }
            targetList := targets.Targets()
            if idx < 0 || idx >= len(targetList) {
                http.Error(rw, "Bad Request", http.StatusBadRequest)
                return
            }
            aggregator.Aggregate([]string{targetList[idx]}, rw, targets)
        } else {
            aggregator.Aggregate(targets.Targets(), rw, targets)
        }

    })
    mux.HandleFunc("/alive", func(rw http.ResponseWriter, r *http.Request) {
        defer r.Body.Close()
        rw.WriteHeader(http.StatusOK)
    })
    mux.HandleFunc("/list_targets", func(rw http.ResponseWriter, r *http.Request) {
        defer r.Body.Close()
        rw.Header().Set("Content-Type", "text/plain")
        targets := strings.Join(targets.Targets(), ",")
        rw.Write([]byte(targets))
    })
    if *dynamicRegistration {
        mux.HandleFunc("/register", func(rw http.ResponseWriter, r *http.Request) {
            defer r.Body.Close()
            err := r.ParseForm()
            if err != nil {
                http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
                return
            }
            name := r.Form.Get("name")
            address := r.Form.Get("address")
            if name == "" || address == "" {
                http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
                return
            }

            schema := r.Form.Get("schema")
            if schema == "" {
                schema = "http"
            }

            uri := schema + "://" + address
            targets.AddTarget(name + "=" + uri)
            log.Printf("Registered target %s with name %s\n", uri, name)
        })
        mux.HandleFunc("/unregister", func(rw http.ResponseWriter, r *http.Request) {
            defer r.Body.Close()
            err := r.ParseForm()
            if err != nil {
                http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
                return
            }
            name := r.Form.Get("name")
            address := r.Form.Get("address")
            if name == "" || address == "" {
                http.Error(rw, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
                return
            }

            schema := r.Form.Get("schema")
            if schema == "" {
                schema = "http"
            }

            uri := schema + "://" + address
            targets.RemoveTarget(name + "=" + uri)
            log.Printf("Unregistered target %s with name %s\n", uri, name)
        })
    }

    log.Printf("Starting server on %s with targets:\n", config.Server.Bind)
    for _, t := range targets.Targets() {
        log.Printf("  - %s\n", t)
    }

    bindServer(config.Server.Bind, mux)
}

func bindServer(bindAddr string, handler http.Handler) {
    log.Fatal(http.ListenAndServe(bindAddr, handler))
}

func NewTargets(initialTargets []string) *Targets {
    t := &Targets{
        targets: make(map[string]struct{}),
        lock:    sync.RWMutex{},
    }
    for _, v := range initialTargets {
        t.AddTarget(v)
    }
    return t
}

type Targets struct {
    cachePath string
    targets   map[string]struct{}
    lock      sync.RWMutex
}

func (t *Targets) AddTarget(target string) {
    target = strings.TrimSpace(target)
    if target == "" {
        return
    }
    t.targets[target] = struct{}{}
}

func (t *Targets) RemoveTarget(target string) {
    target = strings.TrimSpace(target)
    t.lock.Lock()
    defer t.lock.Unlock()
    delete(t.targets, target)
}

func (t *Targets) Targets() []string {
    t.lock.RLock()
    defer t.lock.RUnlock()

    ts := []string{}
    for k := range t.targets {
        ts = append(ts, k)
    }

    return ts
}

type Result struct {
    URL          string
    Name         string
    SecondsTaken float64
    MetricFamily map[string]*io_prometheus_client.MetricFamily
    Error        error
}

type Aggregator struct {
    HTTP *http.Client
}

func (f *Aggregator) Aggregate(targets []string, output io.Writer, targetsInstance *Targets) {
    resultChan := make(chan *Result, 100)

    for _, target := range targets {
        go f.fetch(target, resultChan, targetsInstance)
    }

    func(numTargets int, resultChan chan *Result) {
        numResults := 0
        allFamilies := make(map[string]*io_prometheus_client.MetricFamily)

        for {
            if numTargets == numResults {
                break
            }

            result := <-resultChan
            numResults++

            if result.Error != nil {
                log.Printf("Fetch error: %s", result.Error.Error())
                continue
            }

            for mfName, mf := range result.MetricFamily {
                if *targetLabelsEnabled {
                    for _, m := range mf.Metric {
                        m.Label = append(m.Label, &io_prometheus_client.LabelPair{Name: targetLabelName, Value: &result.Name})
                    }
                }
                if existingMf, ok := allFamilies[mfName]; ok {
                    existingMf.Metric = append(existingMf.Metric, mf.Metric...)
                } else {
                    allFamilies[*mf.Name] = mf
                }
            }
            if *verboseFlag {
                log.Printf("OK: %s=%s was refreshed in %.3f seconds", result.Name, result.URL, result.SecondsTaken)
            }
        }
        encoder := expfmt.NewEncoder(output, "text/plain")
        for _, f := range allFamilies {
            if err := encoder.Encode(f); err != nil {
                log.Printf("Failed to encode family: %s", err.Error())
            }
        }

    }(len(targets), resultChan)
}

func (f *Aggregator) fetch(target string, resultChan chan *Result, targetsInstance *Targets) {
    s := strings.Split(target, "=")
    url := s[0]
    name := s[0]
    if len(s) > 1 {
        url = strings.Join(s[1:], "=")
    }

    startTime := time.Now()
    res, err := f.HTTP.Get(url)

    result := &Result{URL: url, Name: name, SecondsTaken: time.Since(startTime).Seconds(), Error: nil}
    if res != nil {
        result.MetricFamily, err = getMetricFamilies(res.Body)
        if err != nil {
            result.Error = fmt.Errorf("failed to add labels to target %s metrics: %s", target, err.Error())
            resultChan <- result
            targetsInstance.RemoveTarget(target)
            return
        }
    }
    if err != nil {
        result.Error = fmt.Errorf("failed to fetch URL %s due to error: %s", target, err.Error())
        targetsInstance.RemoveTarget(target)
    }
    resultChan <- result
}

func getMetricFamilies(sourceData io.Reader) (map[string]*io_prometheus_client.MetricFamily, error) {
    parser := expfmt.TextParser{}
    metricFamilies, err := parser.TextToMetricFamilies(sourceData)
    if err != nil {
        return nil, err
    }
    return metricFamilies, nil
}

func filterEmptyStrings(ss []string) []string {
    filtered := []string{}
    for _, s := range ss {
        if s != "" {
            filtered = append(filtered, s)
        }
    }
    return filtered
}

