package controller

import (
    "context"
    "fmt"
    "io/ioutil"
    "net/http"
    "strings"
    "time"

    corev1 "k8s.io/api/core/v1"
    metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
    "k8s.io/apimachinery/pkg/runtime"
    ctrl "sigs.k8s.io/controller-runtime"
    "sigs.k8s.io/controller-runtime/pkg/client"
    "sigs.k8s.io/controller-runtime/pkg/log"

    monitorv1 "gitlab.com/optimus3435034/snmp-operator/api/v1"
)

type MonitorConfigReconciler struct {
    client.Client
    Scheme *runtime.Scheme
}

func (r *MonitorConfigReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
    logger := log.FromContext(ctx)
    logger.Info("Reconciling MonitorConfig", "Request.Namespace", req.Namespace, "Request.Name", req.Name)

    var monitorConfig monitorv1.MonitorConfig
    if err := r.Get(ctx, req.NamespacedName, &monitorConfig); err != nil {
        logger.Error(err, "unable to fetch MonitorConfig")
        return ctrl.Result{}, client.IgnoreNotFound(err)
    }

    if !checkAPI(monitorConfig.Spec.APIURL) {
        logger.Info("API check did not return 5 elements, registering all pods in 'snmp-exporter' namespace")

        var pods corev1.PodList
        if err := r.List(ctx, &pods, client.InNamespace(monitorConfig.Spec.TargetNamespace)); err != nil {
            logger.Error(err, "unable to list pods in the target namespace", "Namespace", monitorConfig.Spec.TargetNamespace)
            return ctrl.Result{}, err
        }

        registrationURL := monitorConfig.Spec.RegistrationURL

        for _, pod := range pods.Items {
            apiURL := strings.ReplaceAll(registrationURL, "{name}", pod.Name)
            apiURL = strings.ReplaceAll(apiURL, "{address}", fmt.Sprintf("%s:9116/metrics", pod.Status.PodIP))
            
            _, err := http.Get(apiURL)
            if err != nil {
                logger.Error(err, "Failed to register pod via API", "Pod Name", pod.Name, "Pod IP", pod.Status.PodIP)
            } else {
                logger.Info("Successfully registered pod", "Pod Name", pod.Name, "Pod IP", pod.Status.PodIP)
            }
        }
    } else {
        logger.Info("API check succeeded, found exactly 5 elements")
    }

    monitorConfig.Status.LastChecked = metav1.NewTime(time.Now())
    monitorConfig.Status.Success = true
    if err := r.Status().Update(ctx, &monitorConfig); err != nil {
        logger.Error(err, "Failed to update MonitorConfig status")
        return ctrl.Result{}, err
    }

    return ctrl.Result{RequeueAfter: time.Minute}, nil
}

func checkAPI(url string) bool {
    resp, err := http.Get(url)
    if err != nil {
        return false
    }
    defer resp.Body.Close()
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return false
    }
    return len(strings.Split(string(body), ",")) == 5
}

func (r *MonitorConfigReconciler) SetupWithManager(mgr ctrl.Manager) error {
    logger := ctrl.Log.WithName("setup")
    logger.Info("Setting up controller")
    return ctrl.NewControllerManagedBy(mgr).
        For(&monitorv1.MonitorConfig{}).
        Complete(r)
}

